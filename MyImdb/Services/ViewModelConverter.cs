﻿using MyImdb.Models;
using MyImdb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Services {
	public class ViewModelConverter {
		
		public MovieInfoViewModel ToMovieInfo(Movie movie) {
			return new MovieInfoViewModel() {
				Rank = movie.Rank,
				Title = movie.Title,
				Year = movie.Year,
				StoryLine = movie.Storyline
			};
		}

		public MovieListComingSoon ToMovieListComingSoon(Movie movie) {
			return new MovieListComingSoon() {
				Id = movie.Id,
				Title = movie.Title,
			};
		}
		public IEnumerable<MovieListComingSoon> ToMovieListComingSoon(IEnumerable<Movie> movies) {
			return movies.Select(m => ToMovieListComingSoon(m));
		}

		public IEnumerable<MovieListViewModel> ToMovieList(IEnumerable<Movie> movies) {
			return movies.Select(m => ToMovieList(m));
		}

		public MovieListViewModel ToMovieList(Movie movie) {
			return new MovieListViewModel() {
				Id = movie.Id,
				Rank = movie.Rank,
				Title = movie.Title,
				Year = movie.Year
			};
		}

	}
}
