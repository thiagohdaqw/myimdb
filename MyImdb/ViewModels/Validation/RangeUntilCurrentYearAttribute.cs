﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.ViewModels.Validation {
	public class RangeUntilCurrentYearAttribute : RangeAttribute {

		public RangeUntilCurrentYearAttribute(int min) : base(min, DateTime.Now.Year) { }
	
	}
}
