﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.ViewModels {
	public class IndexViewModel {
		public IEnumerable<MovieListViewModel> MoviesOfTheYear { get; set; }
		public IEnumerable<MovieListComingSoon> MoviesComingSoon { get; set; }

	}
}
