﻿using MyImdb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.ViewModels
{
    public class MovieListViewModel
    {
		public int Id { get; set; }
		public int Rank { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }

		
	}
}
