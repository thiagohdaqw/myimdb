﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyImdb.Models;
using MyImdb.Services;
using MyImdb.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers
{
    public class HomeController : Controller
    {
		private readonly ViewModelConverter vmc;

		public HomeController(ViewModelConverter vmc)
        {
			this.vmc = vmc;
		}

        public IActionResult Index()
        {
			var moviesOfTheYear = vmc.ToMovieList(Movie.SelectAll().FindAll(m => m.Year == DateTime.Now.Year));
			var moviesCoomingSoon = vmc.ToMovieListComingSoon(
				Movie.SelectAll()
				.FindAll(m => m.Year == DateTime.Now.Year)
				.Take(10)
				.OrderByDescending(m => m.Id)
			);
			var indexModel = new IndexViewModel() {
				MoviesOfTheYear = moviesOfTheYear,
				MoviesComingSoon = moviesCoomingSoon
			};
            return View(indexModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string message = null)
        {
            return View(new ErrorViewModel { 
				RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
				Message = message
			});
        }
    }
}
