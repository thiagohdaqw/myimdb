﻿using Microsoft.AspNetCore.Mvc;
using MyImdb.Models;
using MyImdb.Services;
using MyImdb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers
{
    public class MovieController : Controller
    {
		private readonly ViewModelConverter vmc;

		public MovieController(ViewModelConverter vmc) {
			this.vmc = vmc;
		}

        public IActionResult Index(string msg = null){
			var movies = vmc.ToMovieList(Movie.SelectAll());
			ViewBag.Message = msg;
			// Outras formas:
			// tupla
			// novo ViewModel
			// ViewData
            return View(movies);
        }

		public IActionResult MovieInfo(int id) {
			var movie = Movie.SelectAll().FirstOrDefault(m => m.Id == id);
			if (movie == default(Movie)) {
				return RedirectToAction(nameof(HomeController.Error), "Home", new { message = $"Movie id={id} not found!"});
			}

			ViewData["ContentTitle"] = TempData["ContentTitle"];
			return View(vmc.ToMovieInfo(movie));
		}

        public IActionResult MovieOfTheMonth(){
			var movieOfTheMonth = Movie.SelectAll().FirstOrDefault(m => m.Year == DateTime.Now.Year);
			TempData["ContentTitle"] = "Movie of The Month";
            return RedirectToAction(nameof(MovieInfo), new { id = movieOfTheMonth.Id });
        }

		
		public IActionResult Create() {
			return View();
		}

		[HttpPost]
		public	IActionResult Create(MovieCreateViewModel model) {
			if (!ModelState.IsValid) {
				return View(model);
			}
			return RedirectToAction(nameof(Index), new { msg = "Movie created with success." });
		}
	}
}
