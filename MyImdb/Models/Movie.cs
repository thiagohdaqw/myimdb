﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public int Rank { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public string Storyline { get; set; }

        #region RETRIEVE
        public static List<Movie> SelectAll()
        {
            return new List<Movie>()
            {
                new Movie() {
                    Id = 1, Rank = 9, Title = "The Shawshank Redemption", Year = 1994,
                    Storyline = "Two imprisoned men bond over a number of years..."
                },
                new Movie() {
                    Id = 2, Rank = 8, Title = "The Godfather", Year = 1972,
                    Storyline = "The aging patriarch of an organized crime dynasty..."
                },
                new Movie() {
                    Id = 3, Rank = 7, Title = "The Godfather -Part II", Year = 1974,
                    Storyline = "The early life and career of Vito Corleone in 1920s..."
                },
                new Movie() {
                    Id = 4, Rank = 6, Title = "The Dark Knight", Year = 2008,
                    Storyline = "When the menace known as the Joker wreaks havoc..."
                },
				new Movie() {
					Id = 5, Rank = 9, Title = "Spider-Man: No Way Home", Year = 2022,
					Storyline = "With Spider-Man's identity now revealed, our friendly neighborhood web-slinger is unmasked and no longer able to separate his normal life as Peter Parker..."
				},
				new Movie() {
					Id = 6, Rank = 8, Title = "Avatar 2", Year = 2022,
					Storyline = "Jake Sully and Ney'tiri have formed a family and are doing everything to stay together..."
				},
				new Movie() {
					Id = 7, Rank = 7, Title = "The Batman", Year = 2022,
					Storyline = "The Riddler plays a dangerous game of cat and mouse with Batman and Commissioner Gordon..."
				},
				new Movie() {
					Id = 8, Rank = 7, Title = "Moonfall", Year = 2022,
					Storyline = "The world stands on the brink of annihilation when a mysterious force knocks the moon..."
				},
				new Movie() {
					Id = 7, Rank = 7, Title = "The Batman 2", Year = 2022,
					Storyline = "The Riddler plays a dangerous game of cat and mouse with Batman and Commissioner Gordon..."
				},
				new Movie() {
					Id = 8, Rank = 7, Title = "Moonfall 2", Year = 2022,
					Storyline = "The world stands on the brink of annihilation when a mysterious force knocks the moon..."
				},
			};
        }

		#endregion
	}
}
